#!/usr/bin/env sh

do_test() {
  PACKAGE=$1
  cross test --quiet --target $PACKAGE

  if [ "$?" == "0"  ]
    then
      echo -e "\e[32m 😎 test ${PACKAGE} OK\e[0m"
    else
      echo -e "\e[31m 😭---> test ${PACKAGE} NOK\e[0m"
  fi
}

do_test "armv7-unknown-linux-gnueabihf"
do_test "arm-unknown-linux-gnueabihf"
do_test "aarch64-unknown-linux-musl"
do_test "aarch64-unknown-linux-gnu"
# do_test "x86_64-unknown-netbsd"
do_test "x86_64-unknown-linux-musl"
do_test "x86_64-unknown-linux-gnu"
do_test "x86_64-pc-windows-gnu"