#!/usr/bin/env sh

P_DIR=$PWD

VERSION=$1

APP_NAME='sha3sum'

CROSS_CONFIG='./Cross.toml'

if [ -z "$VERSION" ]
  then
    echo -e "\e[31m 😭---> Version should be provided. 1.2.3\e[0m"
    exit 1
fi

if [ -z "$PRIVATE_TOKEN" ]
  then
    echo -e "\e[31m 😭---> Env PRIVATE_TOKEN not provided \e[0m"
    exit 1
fi

PROJECT_URL="http://gitlab.com/api/v4/projects/32465864/packages/generic/${APP_NAME}/${VERSION}/"


do_clean(){
  rm ./packages/*.tar
  rm ./packages/*.tar.gz
  rm ./packages/*.zip
  cargo clean
}

do_posix() {
  PACKAGE=$1
  cross build --release --target $PACKAGE

  if [ "$?" == "0"  ]
    then
      echo -e "\e[32m 😎 build ${PACKAGE} OK\e[0m"
      pushd "./target/${PACKAGE}/release/"
      sha3sum -a 256 ./${APP_NAME} > ./${APP_NAME}.hash
      tar -cvf "${P_DIR}/packages/${APP_NAME}-${PACKAGE}-${VERSION}.tar" ./${APP_NAME} ./${APP_NAME}.hash
      popd
      pushd "./packages/generic_linux/"
      tar -uvf "${P_DIR}/packages/${APP_NAME}-${PACKAGE}-${VERSION}.tar" *.sh
      popd
      gzip -9 "${P_DIR}/packages/${APP_NAME}-${PACKAGE}-${VERSION}.tar"
      curl -L --header "private-token:$PRIVATE_TOKEN" --upload-file "${P_DIR}/packages/${APP_NAME}-${PACKAGE}-${VERSION}.tar.gz" "${PROJECT_URL}${APP_NAME}-${PACKAGE}-${VERSION}.tar.gz"
    else
      echo -e "\e[31m 😭---> build ${PACKAGE} NOK\e[0m"
  fi
}

do_zip() {
  PACKAGE=$1
  EXT=$2
  cross build --release --target $PACKAGE
  if [ "$?" == "0"  ]
      then
        echo -e "\e[32m 😎 build ${PACKAGE} OK\e[0m"
        pushd "./target/${PACKAGE}/release/"
        sha3sum -a 256 ./${APP_NAME}${EXT} > ./${APP_NAME}.hash
        7z -tzip a "${P_DIR}/packages/${APP_NAME}-${PACKAGE}-${VERSION}.zip" ./${APP_NAME}${EXT} ./${APP_NAME}.hash
        popd
        curl -L --header "private-token:$PRIVATE_TOKEN" --upload-file "${P_DIR}/packages/${APP_NAME}-${PACKAGE}-${VERSION}.zip" "${PROJECT_URL}${APP_NAME}-${PACKAGE}-${VERSION}.zip"
      else
        echo -e "\e[31m 😭---> build ${PACKAGE} NOK\e[0m"
    fi
}

do_clean
do_posix "armv7-unknown-linux-gnueabihf"
do_posix "arm-unknown-linux-gnueabihf"
do_posix "aarch64-unknown-linux-musl"
do_posix "aarch64-unknown-linux-gnu"
# do_posix "x86_64-unknown-netbsd"
do_posix "x86_64-unknown-linux-musl"
do_posix "x86_64-unknown-linux-gnu"
do_zip "x86_64-pc-windows-gnu" ".exe"