#!/usr/bin/sh

# Test 1
(echo 'a' | cargo run -- -a Keccak256 -t)
if [ $? -eq 0 ]; then
    echo -e "\e[32m\tTest 1 OK\e[0m";
else
    echo -e "\e[31m\tTest 1 NOK\e[0m";
fi

# Test 2
(cargo run -- -a 256 ./tests/data/f1.raw)
if [ $? -eq 0 ]; then
    echo -e "\e[32m\tTest 2 OK\e[0m";
else
    echo -e "\e[31m\tTest 2 NOK\e[0m";
fi

# Test 3
(cargo run -- -a 256 ./tests/data/f1.raw ./tests/data/f2.raw)
if [ $? -eq 0 ]; then
    echo -e "\e[32m\tTest 3 OK\e[0m";
else
    echo -e "\e[31m\tTest 3 NOK\e[0m";
fi

# Test 4
(cargo run -- -c ./tests/check-file.txt)
if [ $? -eq 0 ]; then
    echo -e "\e[32m\tTest 4 OK\e[0m";
else
    echo -e "\e[31m\tTest 4 NOK\e[0m";
fi

# Test 5
(cargo run -- -c ./tests/check-file_nok.txt)
if [ $? -eq 64 ]; then
    echo -e "\e[32m\tTest 5 OK\e[0m";
else
    echo -e "\e[31m\tTest 5 NOK\e[0m";
fi

# Test 6
(cargo run -- -a 256 ./tests/data/f1_.raw)
if [ $? -eq 64 ]; then
    echo -e "\e[32m\tTest 6 OK\e[0m";
else
    echo -e "\e[31m\tTest 6 NOK\e[0m";
fi

# Test 7
(cargo run -- -a 255 ./tests/data/f1.raw)
if [ $? -eq 2 ]; then
    echo -e "\e[32m\tTest 7 OK\e[0m";
else
    echo -e "\e[31m\tTest 7 NOK\e[0m";
fi

# Test 8
(cargo run -- -a 256 -c ./tests/check-file_nok_2.txt)
if [ $? -eq 2 ]; then
    echo -e "\e[32m\tTest 8 OK\e[0m";
else
    echo -e "\e[31m\tTest 8 NOK\e[0m";
fi

# Test 9 issue #3 https://gitlab.com/kurdy/sha3sum/-/issues/3
(cargo run -- -a 256 --tag ./tests/data/f1.raw)
if [ $? -eq 0 ]; then
    echo -e "\e[32m\tTest 9 OK\e[0m";
else
    echo -e "\e[31m\tTest 9 NOK\e[0m";
fi

# Test 10 issue #3 https://gitlab.com/kurdy/sha3sum/-/issues/3
(cargo run -- -a 256 -b --tag ./tests/data/f1.raw)
if [ $? -eq 0 ]; then
    echo -e "\e[32m\tTest 10 OK\e[0m";
else
    echo -e "\e[31m\tTest 10 NOK\e[0m";
fi

# Test 11
r1=$(cargo run -- -a 512 ./tests/data/f1.raw)
r2=$(cargo run -- -a 512 -b ./tests/data/f1.raw)
if [ $r1 == $r2 ]; then
    echo -e "\e[32m\tTest 11 OK\e[0m";
else
    echo -e "\e[31m\tTest 11 NOK\e[0m";
fi