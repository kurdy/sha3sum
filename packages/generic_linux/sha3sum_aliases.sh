#!/bin/sh
# Aliases to use with sha3sum
# https://gitlab.com/kurdy/sha3sum
alias sha3_512sum="sha3sum -a 512"
alias sha3_384sum="sha3sum -a 384"
alias sha3_256sum="sha3sum -a 256"
alias sha3_224sum="sha3sum -a 224"
alias keccak_512sum="sha3sum -a keccak512"
alias keccak_384sum="sha3sum -a keccak384"
alias keccak_256sum="sha3sum -a keccak256"
alias keccak_256Fullsum="sha3sum -a keccak256full"
alias keccak_224sum="sha3sum -a keccak224"