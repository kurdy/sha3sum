#!/bin/sh
# Generic linux install for sha3sum
# https://gitlab.com/kurdy/sha3sum

[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"
BIN_PATH='/usr/local/bin'
ALIASES_PATH='/etc/aliases.d'
PROFILES_PATH='/etc/profile.d'

set -e

if [ -d "$BIN_PATH" ]; then
  cp sha3sum $BIN_PATH/
  chmod +x $BIN_PATH/sha3sum
fi

if [ -d "$ALIASES_PATH" ]; then
    cp sha3sum_aliases.sh $ALIASES_PATH/
    chmod u+x $ALIASES_PATH/sha3sum_aliases.sh
elif [ -d "PROFILES_PATH" ]; then
    cp sha3sum_aliases.sh PROFILES_PATH/
    chmod u+x PROFILES_PATH/sha3sum_aliases.sh
fi