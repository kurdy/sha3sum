#!/bin/sh
# Generic linux uninstall for sha3sum
# https://gitlab.com/kurdy/sha3sum

[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"

BIN_PATH='/usr/local/bin/sha3sum'
ALIASES_PATH='/etc/aliases.d/sha3sum_aliases.sh'
PROFILE_PATH='/etc/profile.d/sha3sum_aliases.sh'

set -e

if [ -f "$BIN_PATH" ]; then
  rm $BIN_PATH
fi

if [ -f "$ALIASES_PATH" ]; then
  rm $ALIASES_PATH
fi

if [ -f "$PROFILE_PATH" ]; then
  rm $PROFILE_PATH
fi