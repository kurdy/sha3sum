#!/bin/sh
# Helper to create binary.
# https://gitlab.com/kurdy/sha3sum
# ./helper_to_create_binary.sh 1.1.5 x86_64-Linux

VERSION=$1
ARCH=$2

if [ -z "$ARCH" ]; then
    ARCH="$(uname -i)"
    echo "Machine: $ARCH"
fi
FILE_NAME="sha3sum-$ARCH-$VERSION.tar.gz"

cd ./generic_linux
cp ../../target/release/sha3sum ./;
strip ./sha3sum
./sha3sum -a 256 ./sha3sum > sha3sum.hash;
./sha3sum -c sha3sum.hash;
tar -cvzf "$FILE_NAME" --exclude *.tar.gz *;

rm sha3sum sha3sum.hash