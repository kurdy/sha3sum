![Licence](https://img.shields.io/crates/l/sha3sum)
![Version](https://img.shields.io/crates/v/sha3sum)
[![dependency status](https://deps.rs/repo/gitlab/kurdy/sha3sum/status.svg)](https://deps.rs/repo/gitlab/kurdy/sha3sum)
![Download.io](https://img.shields.io/crates/d/sha3sum)
[![pipeline](https://gitlab.com/kurdy/sha3sum/badges/master/pipeline.svg)](https://gitlab.com/kurdy/sha3sum)

# sha3 utilities: Print or check SHA3 digests
Command line that wraps sha3 lib from [RustCrypto/hashes](https://github.com/RustCrypto/hashes).  
Commands and options are similar to the GNU Linux command shaXXXsum.

One of the goals of this project is to provide a cross-platform solution that does not require external dependencies.

## Install
* Using rust toolschain:  `cargo install sha3sum`
* Otherwise see [Wiki](https://gitlab.com/kurdy/sha3sum/-/wikis/home)

### Binaries

Binary versions are available in the [Package Registry](https://gitlab.com/kurdy/sha3sum/-/packages) area. 

## Howto
* Get help:  `sha3sum --help`
* Create for a file an hash Sha3-256  `sha3sum -a 256 <path to file>`
* Create for all file in repository with Keccak512  `sha3sum -a Keccak512 <path>`
* Create a hash for a text file using text mode  `sha3sum -a 384 -t <path to file>`
* Create for a file a hash Sha3-256 with output BSD style  `sha3sum -a 256 --tag <path to file>`
* Read SHA3 sums from the FILEs and check them  `sha3sum -c <path to file>`




## Tests

All releases are build and tested using following OS: 

* Linux: x86_64
* Linux aarch64
* Windows: x86_64

[Sources from GitLab](https://gitlab.com/kurdy/sha3sum.git) `git clone https://gitlab.com/kurdy/sha3sum.git`
